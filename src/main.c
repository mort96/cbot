#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <pipe> <str>\n", argv[0]);
		return EXIT_FAILURE;
	}

	char *pipe = argv[1];
	char *str = argv[2];
	int ret = EXIT_SUCCESS;

	int fd = open(pipe, O_RDWR);
	if (fd < 0) {
		perror(pipe);
		return EXIT_FAILURE;
	}

	size_t len = strlen(str);
	uint32_t len_nl = htonl(len);
	ssize_t wr = write(fd, &len_nl, sizeof(len_nl));
	if (wr < 0) {
		perror("write");
		wr = EXIT_FAILURE;
		goto cleanup;
	} else if ((size_t)wr < sizeof(len_nl)) {
		fprintf(stderr, "Wrote too few bytes (%zi < %zi)\n", wr, sizeof(len_nl));
		ret = EXIT_FAILURE;
		goto cleanup;
	}

	wr = write(fd, str, len);
	if (wr < 0) {
		perror("write");
		wr = EXIT_FAILURE;
		goto cleanup;
	} else if ((size_t)wr < sizeof(len_nl)) {
		fprintf(stderr, "Wrote too few bytes (%zi < %zi)\n", wr, len);
		ret = EXIT_FAILURE;
		goto cleanup;
	}

	fprintf(stderr, "Wrote data.\n");

cleanup:
	close(fd);
	return ret;
}
