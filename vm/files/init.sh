#!/bin/sh
set -e

cd ~

echo "Updating..."
sudo apt update
sudo apt upgrade
echo

echo "Installing packages..."
sudo apt install gcc make
echo

echo "Setting up auto login..."
sudo mkdir -p /etc/systemd/system/getty@tty1.service.d
echo "/etc/systemd/system/getty@tty1.service.d/override.conf:"
echo "[Service]
ExecStart=
ExecStart=-/sbin/agetty --noissue --autologin $USER %I \$TERM
Type=idle" \
	| sudo tee /etc/systemd/system/getty@tty1.service.d/override.conf
echo

echo "Disabling unnecessary systemd services..."
sudo systemctl mask snapd lxd-containers thermald apport ssh icsid
echo

echo "Setting up service..."
sudo usermod -aG dialout "$USER"
cp -r /mnt/service .
sudo chown -R "$USER":"$(id -gn)" service
echo ".bash_profile:"
echo '[ "$(tty)" = "/dev/tty1" ] && cd service && ./init.sh' | tee .bash_profile
echo

echo "Compiling service"
cd service
make
cd ..

echo "Setup complete, hit enter to shut down."
read _
shutdown -h now
