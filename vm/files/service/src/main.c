#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <poll.h>
#include <arpa/inet.h>
#include <termios.h>
#include <string.h>

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <device>\n", argv[0]);
		return EXIT_FAILURE;
	}

	char *data = NULL;
	size_t data_size = 0;
	int devfd = -1;

	char *device = argv[1];
	devfd = open(device, O_RDWR);
	if (devfd < 0) {
		perror(device);
		return EXIT_FAILURE;
	}

	// Configure TTY
	{
		struct termios tty;
		memset(&tty, 0, sizeof(tty));
		if (tcgetattr(devfd, &tty) < 0) {
			perror("tcgetattr");
			goto fail;
		}

		cfsetispeed(&tty, B115200);
		cfsetospeed(&tty, B115200);

		tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
		tty.c_cflag &= ~CSIZE;
		tty.c_cflag |= CS8;         /* 8-bit characters */
		tty.c_cflag &= ~PARENB;     /* no parity bit */
		tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
		tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

		/* setup for non-canonical mode */
		tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
		tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
		tty.c_oflag &= ~OPOST;

		/* fetch bytes as they become available */
		tty.c_cc[VMIN] = 1;
		tty.c_cc[VTIME] = 1;

		if (tcsetattr (devfd, TCSANOW, &tty) != 0) {
			perror("tcsetattr");
			goto fail;
		}
	}

	struct pollfd fds[1];
	fds[0].fd = devfd;
	fds[0].events = POLLIN;

	size_t left_to_read = 0;
	size_t len = 0;

	while (1) {
		int ret = poll(fds, 1, -1);
		if (ret < 0) {
			perror("poll");
			goto fail;
		}
		if (ret == 0)
			continue;

		// Receive length header if we're at the start of a new message
		if (left_to_read == 0) {
			uint32_t len_nl;
			ssize_t rd = read(devfd, &len_nl, sizeof(len_nl));
			if (rd < 0) {
				perror("read");
				goto fail;
			}
			len = ntohl(len_nl);
			left_to_read = len;
			fprintf(stderr, "Should read %zu bytes.\n", len);

			// We don't want to read more than, say, 1MB
			if (len > 1000 * 1000) {
				fprintf(stderr, "Read length too big.\n");
				goto fail;
			}

			// Realloc memory if necessary
			if (len + 1 > data_size) {
				data_size = len + 1;
				data = realloc(data, data_size);
				if (data == NULL) {
					fprintf(stderr, "Memory allocation failed.\n");
					goto fail;
				}
			}
		}

		// Read data
		ssize_t rd = read(devfd, data + (len - left_to_read), left_to_read);
		if (rd < 0) {
			perror("read");
			goto fail;
		}
		if (rd == 0) {
			fprintf(stderr, "Empty read. Stream might have closed.\n");
			goto fail;
		}
		left_to_read -= rd;

		if (left_to_read == 0) {
			data[len] = '\0';
			fprintf(stderr, "Received data: '%s'\n", data);
		} else {
			fprintf(stderr, "Read %zi bytes, %zi bytes left.\n", rd, left_to_read);
		}
	}

fail:
	if (data != NULL)
		free(data);
	if (devfd >= 0)
		close(devfd);
	return EXIT_FAILURE;
}
