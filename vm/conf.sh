IMG=vm.img
ISO="ubuntu-18.04.1-live-server-amd64.iso"
ISO_DL="http://releases.ubuntu.com/18.04.1/ubuntu-18.04.1-live-server-amd64.iso"
RAM=300M
RAM_INIT=512M
SIZE=8G
QEMU="qemu-system-x86_64 -enable-kvm -curses -drive format=qcow2,file=$IMG -m $RAM"
QEMU_INIT="qemu-system-x86_64 -enable-kvm -curses -drive format=qcow2,file=$IMG -m $RAM_INIT"
