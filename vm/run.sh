#!/bin/sh
set -e

. ./conf.sh

mkfifo serial
echo $QEMU -boot c -serial pipe:serial
$QEMU -boot c -serial pipe:serial
rm -f serial
