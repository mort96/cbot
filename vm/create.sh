#!/bin/sh
set -e

. ./conf.sh

if [ "$1" == "new" ]; then
	echo "This script will create '$IMG' of size $SIZE from $ISO."
	echo "Create a user with whatever name you want, it doesn't matter."
	echo "Press enter to continue.."
	read _

	if [ "$2" != "no-dl" ]; then
		echo "Downloading $ISO..."
		rm -f "$ISO"
		wget -O "$ISO" "$ISO_DL"
	fi

	rm -f "$IMG" "$IMG.clean"
	qemu-img create -f qcow2 "$IMG" "$SIZE"
	$QEMU_INIT -no-reboot -boot d -cdrom "$ISO"

	echo "Backing up $IMG to $IMG.clean..."
	cp "$IMG" "$IMG.clean"
else
	if ! [ -e "$IMG.clean" ]; then
		echo "File '$IMG.clean' doesn't exist. Did you mean '$0 new'?"
		exit 1
	fi
	echo "Copying $IMG.clean to $IMG..."
	cp "$IMG.clean" "$IMG"
fi

echo "Log in and run this command to set up the VM:"
echo "sudo mount -t 9p -o trans=virtio,version=9p2000.L host0 /mnt && /mnt/init.sh"
echo "Press enter to start the VM."
read _

$QEMU_INIT -boot c \
	-virtfs local,path=files,mount_tag=host0,security_model=passthrough,id=host0

echo "If you executed the aforementioned command, and it completed successfully,"
echo "your system should now be completely set up."
echo "Run ./run.sh to start the VM."
